<?php

/**
 * @file
 * Install and uninstall functions for the metatag_taxonomy_facets module.
 */

/**
 * Implements hook_schema().
 */
function metatag_taxonomy_facets_schema() {
  $schema = array();

  $schema['taxonomy_facets_combinations'] = array(
    'description' => 'Table for storing taxonomy facets combinations',
    'fields' => array(
      'cid' => array(
        'description' => 'Primary Key: Unique combination ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'combination_hash' => array(
        'description' => 'combination hash',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'default' => '',
      ),
      'searcher' => array(
        'description' => 'searcher name',
        'type' => 'varchar',
        'length' => 256,
        'not null' => FALSE,
        'default' => '',
      ),
      'facets' => array(
        'description' => 'facets names delimited by semicolons',
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
        'default' => '',
      ),
    ),
    'primary key' => array('cid'),
    'indexes' => array(
      'combinations' => array('combination_hash'),
    ),
  );

  $schema['taxonomy_facets_items'] = array(
    'description' => 'Table for taxonomy facets combinations instances',
    'fields' => array(
      'tfid' => array(
        'description' => 'Primary Key: Unique combination ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'combination_id' => array(
        'description' => 'Combination Id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'tids' => array(
        'description' => 'ids of terms for corresponding to active facets delimited by commas',
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
        'default' => '',
      ),
      'tids_hash' => array(
        'description' => 'tids hash',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'default' => '',
      ),
    ),
    'primary key' => array('tfid'),
    'indexes' => array(
      'facets' => array('combination_id', 'tids_hash'),
    ),
    'foreign keys' => array(
      'taxonomy_facets_combinations' => array(
        'table' => 'taxonomy_facets_combinations',
        'columns' => array(
          'combination_id' => 'cid',
        ),
      ),
    ),
  );

  return $schema;
}

/**
 * Enables metatags support for taxonomy_facets entities.
 * Implements hook_install().
 */
function metatag_taxonomy_facets_install() {
  variable_set('metatag_enable_taxonomy_facets', TRUE);
  variable_set('metatag_enable_taxonomy_facets__taxonomy_facets', TRUE);
}

/**
 * Implements hook_uninstall().
 */
function metatag_taxonomy_facets_uninstall() {
  variable_del('metatag_taxonomy_facets_selected');
  variable_del('metatag_taxonomy_facets_unselected');
  variable_del('metatag_taxonomy_facets_single_facets');
  variable_del('metatag_taxonomy_facets_ignore_unsupported_facets');
}

/**
 * Enable taxonomy_facets entity metatags support
 */
function metatag_taxonomy_facets_update_7104() {
  variable_set('metatag_enable_taxonomy_facets', TRUE);
  variable_set('metatag_enable_taxonomy_facets__taxonomy_facets', TRUE);
}

/**
 * Fixing metatag instances ids (making them shorter and more reliable)
 */
function metatag_taxonomy_facets_update_7105() {
  $metatag_configs = db_select('metatag_config', 'm')
    ->fields('m', array('cid', 'instance'))
    ->condition('m.instance', 'taxonomy_facets:%', 'LIKE')
    ->execute()->fetchAll();

  foreach ($metatag_configs as $config) {
    $prev_instance = substr($config->instance, strlen('taxonomy_facets:'));
    $parts = explode('+', $prev_instance);
    $searcher_name = array_shift($parts);
    $facets = implode('+', $parts);

    $hash = _metatag_taxonomy_facets_get_combination_hash($searcher_name, $facets);
    $instance = _metatag_taxonomy_facets_get_metatag_instance($searcher_name, $hash);

    db_update('metatag_config')
      ->fields(array('instance' => $instance))
      ->condition('cid', $config->cid)
      ->execute();
  }
}

/**
 * Removing unused database table column.
 */
function metatag_taxonomy_facets_update_7106() {
  if (db_index_exists('taxonomy_facets_combinations', 'searcher')) {
    db_drop_index('taxonomy_facets_combinations', 'searcher');
  }
  if (db_field_exists('taxonomy_facets_combinations', 'searcher_hash')) {
    db_drop_field('taxonomy_facets_combinations', 'searcher_hash');
  }
}